﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Savable/Bool")]
public class SavableBoolVariable : SavableScriptable
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public bool Value;

    public override void InjectValue(SavableScriptable value)
    {
        var variable = value as SavableBoolVariable;
        if (variable == null)
            return;

        Value = variable.Value;
    }

    public override void Reset()
    {
        Value = false;
    }

    public void SetValue(bool value)
    {
        Value = value;
    }

    public void SetValue(SavableBoolVariable value)
    {
        Value = value.Value;
    }
}