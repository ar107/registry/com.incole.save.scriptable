﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Savable/Int")]
public class SavableIntVariable : SavableScriptable
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public int Value;

    public override void InjectValue(SavableScriptable value)
    {
        var variable = value as SavableIntVariable;
        if (variable == null)
            return;

        Value = variable.Value;
    }

    public override void Reset()
    {
        Value = -1;
    }

    public void SetValue(int value)
    {
        Value = value;
    }

    public void SetValue(SavableIntVariable value)
    {
        Value = value.Value;
    }
}