﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class SavableScriptable : ScriptableObject
{
    [InlineButton("ChangeId", "new")]
    public string Id = Guid.NewGuid().ToString();

    public void ChangeId()
    {
        Id = Guid.NewGuid().ToString();
    }

    public abstract void InjectValue(SavableScriptable value);

    [Button]
    public abstract void Reset();
}