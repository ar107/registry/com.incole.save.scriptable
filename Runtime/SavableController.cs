﻿using System.Linq;
using UnityEngine;

public class SavableController: MonoBehaviour
{
    public string SaveFile = "State.data";
    public bool LoadOnAwake = true;
    public SavableScriptable[] Savables;
    public StorageService Storage;

    private void Awake()
    {
        Storage = new StorageService(new FileDatabase(Application.persistentDataPath));

        for (int i = 0; i < Savables.Length; i++)
            Savables[i].Reset();

        if (!LoadOnAwake)
            return;

        LoadState();
    }

    public void LoadState()
    {
        Debugr.Log(this, "[State Load] Started");

        var savedState = Storage.Load<StateSave>(SaveFile);

        if (savedState == null)
        {
            Debugr.Log(this, "[State Load] No state");
            return;
        }

        if (savedState.Savables == null)
        {
            Debugr.Log(this, "[State Load] No savables");
            return;
        }

        if (Savables.Length == 0)
        {
            Debugr.Log(this, "[State Load] No savables in app");
            return;
        }

        var doubles = Savables.GroupBy(x => x.Id)
            .Select(x => new { Id = x.Key, Count = x.Count() })
            .Where(x => x.Count > 1)
            .ToArray();

        foreach (var item in doubles)
            Debug.LogError($"There is a savable double {item.Id}");

        for (int i = 0; i < savedState.Savables.Length; i++)
        {
            var saved = savedState.Savables[i];
            var variable = Savables.FirstOrDefault(x => x.Id == saved.Id);

            if (variable == null)
                continue;

            variable.InjectValue(saved);
        }

        Debugr.Log(this, $"[State Load] Complete. Savables: {savedState.Savables.Length}");
    }

    public void SaveState()
    {
        Debugr.Log(this, "[State Save] Started");

        var save = new StateSave
        {
            Savables = Savables
        };

        Storage.Save(save, SaveFile);

        Debugr.Log(this, $"[State Save] Completed. Savables: {save.Savables.Length}");
    }

    public void ResetState()
    {
        foreach (var item in Savables)
            item.Reset();
    }

    public void ErasePersistantStorage()
    {
        System.IO.Directory.Delete(Application.persistentDataPath, true);
        Debugr.Log(this, $"Storage deleted: {Application.persistentDataPath}");
    }
}

public class StateSave
{
    public SavableScriptable[] Savables;
}